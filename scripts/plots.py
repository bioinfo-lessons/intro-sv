import glob
import pandas as pd
import os

chrs = {
    '1': {
        'size': 248956422,
        'genes': {}
    },
    '2': {
        'size': 242193529,
        'genes': {}
    },
    '3': {
        'size': 198295559,
        'genes': {}
    },
    '4': {
        'size': 190214555,
        'genes': {}
    },
    '5': {
        'size': 181538259,
        'genes': {}
    },
    '6': {
        'size': 170805979,
        'genes': {}
    },
    '7': {
        'size': 159345973,
        'genes': {}
    },
    '8': {
        'size': 145138636,
        'genes': {
            'MYC': (127736069,127741434,"up")
        }
    },
    '9': {
        'size': 138394717,
        'genes': {}
    },
    '10': {
        'size': 133797422,
        'genes': {
            'PTEN': (87863113,87971930,"down")
        }
    },
    '11': {
        'size': 135086622,
        'genes': {}
    },
    '12': {
        'size': 133275309,
        'genes': {}
    },
    '13': {
        'size': 114364328,
        'genes': {
            'BRCA2': (32315474,32400266,"down"),
            'RB1': (48303775,48481986,"down")
        }
    },
    '14': {
        'size': 107043718,
        'genes': {}
    },
    '15': {
        'size': 101991189,
        'genes': {}
    },
    '16': {
        'size': 90338345,
        'genes': {}
    },
    '17': {
        'size': 83257441,
        'genes': {}
    },
    '18': {
        'size': 80373285,
        'genes': {}
    },
    '19': {
        'size': 58617616,
        'genes': {}
    },
    '20': {
        'size': 64444167,
        'genes': {}
    },
    '21': {
        'size': 46709983,
        'genes': {}
    },
    '22': {
        'size': 50818468,
        'genes': {}
    },
    'X': {
        'size': 156040895,
        'genes': {}
    },
    'Y': {
        'size': 57227415,
        'genes': {}
    }
}

enabledchrs = ['8','10','13']

for fname in glob.glob("out/predictions/*_bins.bed"):

    sid = fname.split("/")[-1].replace("_bins.bed","")
    bins = pd.read_csv(fname,sep="\t")
    segs = pd.read_csv(fname.replace("bins","segments"),sep="\t")
    for chrn in enabledchrs:
        chrname = str(chrn)
        ax = bins[bins.chr == chrname].plot(x="start",y="ratio",kind="line", style=".",figsize=(16,8),title="{sid} - chr{chrname}".format(sid=sid,chrname=chrname))

        ax.axhline(0,0,ls="dashed")
        
        s = segs[segs.chr == chrname]

        s_up = s[(s.ratio > 0) & (s.zscore >= 5)]
        s_down = s[(s.ratio < 0) & (s.zscore <= 5)]

        ax.hlines(s.ratio,s.start,s.end,linestyle="dotted")
        ax.hlines(s_down.ratio,s_down.start,s_down.end,color='r')
        ax.hlines(s_up.ratio,s_up.start,s_up.end,color='b')
        
        for gene,coords in chrs[chrname]["genes"].items():
            start,end,d = coords
            ax.axvline(start)
            ax.axvline(end)
            ax.text(start + 1000000,1,gene,bbox=dict(facecolor='red', alpha=0.8))

        d = "out/predictions/{sid}.plots".format(sid=sid)
        if not os.path.exists(d):
            os.makedirs(d)
        fig = ax.get_figure()
        fig.savefig('{d}/chr{chrn}_custom.png'.format(d=d,chrn=chrn))
