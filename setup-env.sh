export WD=~/intro-sv
mkdir -p $WD
cd $WD

wget https://bioinformatics.cnio.es/data/courses/intro-sv/data.tar.bz2
tar xjvf data.tar.bz2
rm data.tar.bz2

mamba create -y -n sv wisecondorx parallel pandas matplotlib igv samtools bedtools
